﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class LensEffect : ImageEffectBase {

	public void OnRenderImage (RenderTexture source, RenderTexture destination) {
		Texture2D tex2D = new Texture2D (source.width, source.height);
		tex2D.ReadPixels (new Rect (0, 0, source.width, source.height), 0, 0);
		tex2D.Apply ();
		Color crtColor;
		float crtLuminance, totalLum = 0;
		for (int i = 0; i < tex2D.height; ++i)
		for (int j = 0; j < tex2D.width; ++j) {
			crtColor = tex2D.GetPixel(i, j);
			crtLuminance = (0.299f * crtColor.r + 0.587f * crtColor.g + 0.114f * crtColor.b);
			totalLum += crtLuminance;
		}
		totalLum = totalLum / (tex2D.width * tex2D.height);
		float intensity = (totalLum - 0.4f) / 0.2f * 0.4f + 0.2f;
		Debug.Log ("luminance for current frame is: " + totalLum + "; computed intensity: " + intensity);
		material.SetColor("_LensColor", new Color(0.15f, 0.04f, 0f, 1f));
		material.SetFloat ("_LensIntensity", intensity);
		//		material.SetTextureScale ("_LensMask", new Vector2 (0.5f, 0.5f));
		material.SetTexture("_LensMask", Resources.Load ("LensMask") as Texture);
		Debug.Log ("texture width: " + source.width + "; height: " + source.height);
		Graphics.Blit(source, destination, material);
	}
}

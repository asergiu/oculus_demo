Shader "LensEffect" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_LensMask ("Lens Mask", 2D) = "white" {}
	_LensColor ("Lens Color", Color) = (0, 0, 0, 1)
	_LensIntensity ("Lens Intensity", float) = .5
}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }
				
CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest 
#include "UnityCG.cginc"

uniform sampler2D _MainTex;
uniform sampler2D _LensMask;
uniform fixed4 _LensColor;
uniform float _LensIntensity;

fixed4 frag (v2f_img i) : COLOR
{	

	fixed4 original = tex2D(_MainTex, i.uv);
	fixed4 lensMask = tex2D(_LensMask, fixed2(i.uv.x, 1.0 - i.uv.y));
	
	fixed4 output;;
	if (lensMask.a > 0) {
		output = original * (1 - _LensIntensity) + _LensColor * _LensIntensity;
	} else {
		output = original;
	}
//	output.a = 0;
	
	return output;
}
ENDCG

	}
}

Fallback off

}